import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {

  activeDashboard = '';
  activeVehicle = '';
  activeReport = '';

  constructor(private route: ActivatedRoute){}

  ngOnInit() {
    this.activeDashboard = 'active';
   }

   onActiveDashboard() {
    this.activeDashboard = 'active';
    this.activeVehicle = '';
    this.activeReport = '';
   }

   onActiveVehicle() {
    this.activeVehicle = 'active';
    this.activeDashboard = '';
    this.activeReport = '';
   }

   onActiveReport() {
    this.activeReport = 'active';
    this.activeDashboard = '';
    this.activeVehicle = '';
   }

}

import { ShowAlertService } from './services/show-alert.service';
import { HttpErrorResponse, HttpRequest, HttpHandler } from '@angular/common/http';
import { ErrorHandler,
         Injectable,
         NgZone } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class AplicationErrorHandler extends ErrorHandler {

    constructor(private showAlertService: ShowAlertService,
                private spinner: NgxSpinnerService,
                private zone: NgZone) {
        super();
    }

     handleError(errorResponse: HttpErrorResponse | any) {
        if (errorResponse instanceof HttpErrorResponse) {
            const message = errorResponse.error.message;
            this.zone.run(() => {
                switch (errorResponse.status) {
                        case 400:
                            this.showAlertService.showAlert(message, '#FF0000');
                            break;
                        case 404:
                            this.showAlertService.showAlert(message ||
                                    'Recurso não encontrado. Verifique o console para mais detalhes', '#FF0000');
                            break;
                        case 401:
                            this.showAlertService.showAlert(message, '#FF0000');
                            break;
                        case 500:
                            this.showAlertService.showAlert(message, '#FF0000');
                            break;
                        default:
                            this.showAlertService.showAlert('Erro interno no servidor', '#FF0000');
                    }
            });
        }
        this.spinner.hide();
        super.handleError(errorResponse);
    }
}

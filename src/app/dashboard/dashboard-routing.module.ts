import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

export const DashboardRouting: Routes = [
  {
    path: '',
    component: DashboardComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DashboardRouting)
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule {
}

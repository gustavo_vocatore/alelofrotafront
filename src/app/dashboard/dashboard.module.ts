import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { DashboardRoutingComponent } from './dashboard-routing.component';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [
    SharedModule.forRoot(),
    DashboardRoutingModule
  ],
  declarations: [
    DashboardRoutingComponent,
    DashboardComponent
  ],
  providers: [
  ]
})
export class DashboardModule { }

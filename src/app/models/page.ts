import { Vehicle } from './vehicle';
export class Page {

    constructor() {}

    public first?: boolean;
    public last?: boolean;
    public sort?: string;
    public numberOfElements?: number;
    public number?: number;
    public totalPages?: number;
    public totalElements?: number;
    public hasContent?: boolean;
    public hasPrevious?: boolean;
    public nextPageable?: any;
    public previousPageable?: string;
    public content: Array<Vehicle> = new Array<Vehicle>();
}

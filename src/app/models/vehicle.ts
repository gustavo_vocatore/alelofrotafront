export class Vehicle {

    constructor(registerForm: any) {
        this.plate = registerForm.plate.toUpperCase();
        this.model = registerForm.model;
        this.manufacturer = registerForm.manufacturer;
        this.color = registerForm.color;
        this.status = registerForm.status;
    }

    public id?: number;
    public plate?: string;
    public model?: string;
    public manufacturer?: string;
    public color?: string;
    public status?: boolean;
}

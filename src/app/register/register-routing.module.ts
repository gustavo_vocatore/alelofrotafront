import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register.component';

export const RegisterRouting: Routes = [
  {
    path: '',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(RegisterRouting)
  ],
  exports: [
    RouterModule
  ]
})
export class RegisterRoutingModule {
}

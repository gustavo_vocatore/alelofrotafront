import { ShowAlertService } from './../services/show-alert.service';
import { MessageResponse } from './../models/message-response';
import { Vehicle } from './../models/vehicle';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VehicleService } from './../services/vehicle.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

   // convenience getter for easy access to form fields
   get f() { return this.registerForm.controls; }

  constructor(private spinner: NgxSpinnerService,
              private vehicleService: VehicleService,
              private formBuilder: FormBuilder,
              private showAlertService: ShowAlertService) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      plate: this.formBuilder.control('', [Validators.required]),
      model: this.formBuilder.control('', [Validators.required]),
      manufacturer: this.formBuilder.control('', [Validators.required]),
      color: this.formBuilder.control('', [Validators.required]),
      status: this.formBuilder.control('true', [Validators.required]),
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
        return;
    }
    this.save(this.registerForm.value);
  }

  save(registerForm: any) {
    this.spinner.show();
    const vehicle: Vehicle = new Vehicle(registerForm);
    this.vehicleService.save(vehicle)
      .subscribe((messageResponse: MessageResponse) => {
        this.showAlertService.showAlert(messageResponse.message, '#088A4B');
        this.spinner.hide();
        this.registerForm.reset();
     });
  }

  resetForm(){
    this.registerForm.reset();
  }


}

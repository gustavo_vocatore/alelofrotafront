import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterRoutingComponent } from './register-routing.component';
import { RegisterComponent } from './register.component';

@NgModule({
  imports: [
    SharedModule.forRoot(),
    RegisterRoutingModule
  ],
  declarations: [
    RegisterRoutingComponent,
    RegisterComponent
  ],
  providers: [
  ]
})
export class RegisterModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportComponent } from './report.component';

export const ReportRouting: Routes = [
  {
    path: '',
    component: ReportComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ReportRouting)
  ],
  exports: [
    RouterModule
  ]
})
export class ReportRoutingModule {
}

import { SharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';
import { ReportRoutingModule } from './report-routing.module';
import { ReportRoutingComponent } from './report-routing.component';
import { ReportComponent } from './report.component';

@NgModule({
  imports: [
    SharedModule.forRoot(),
    ReportRoutingModule
  ],
  declarations: [
    ReportRoutingComponent,
    ReportComponent
  ],
  providers: [
  ]
})
export class ReportModule { }

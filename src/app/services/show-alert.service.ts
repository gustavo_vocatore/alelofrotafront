import { Injectable } from '@angular/core';

import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ShowAlertService {

  constructor() { }

  public showAlert(message: string, cor: string) {
    Swal.fire({
        position: 'bottom-left',
        showConfirmButton: true,
        timer: 10000,
        background: cor,
        toast: true,
        confirmButtonText: message,
        confirmButtonColor: cor
       });
  }
}

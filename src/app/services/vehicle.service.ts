import { Page } from './../models/page';
import { MessageResponse } from './../models/message-response';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MEAT_API } from '../app.api';
import { Vehicle } from '../models';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private http: HttpClient) { }

  findByPage(pagina: number): Observable<Page> {
    return this.http.get<Page>(`${MEAT_API}/vehicle?page=` + pagina + '&limit=5');
  }

  findByPlate(plate: string): Observable<Array<Vehicle>> {
    return this.http.get<Array<Vehicle>>(`${MEAT_API}/vehicle?filter=` + plate);
  }

  findByStatus(status: string): Observable<Array<Vehicle>> {
    return this.http.get<Array<Vehicle>>(`${MEAT_API}/vehicle?filter=` + status);
  }

  findById(id: number): Observable<Vehicle> {
    return this.http.get<Vehicle>(`${MEAT_API}/vehicle/` + id);
  }

  save(vehicle: Vehicle): Observable<MessageResponse> {
    return this.http.post<MessageResponse>(`${MEAT_API}/vehicle`, vehicle);
  }

  update(vehicle: Vehicle, id: number): Observable<MessageResponse> {
    return this.http.put<MessageResponse>(`${MEAT_API}/vehicle/` + id, vehicle);
  }

  delete(id: number): Observable<MessageResponse> {
    return this.http.delete<MessageResponse>(`${MEAT_API}/vehicle/` + id);
  }
}

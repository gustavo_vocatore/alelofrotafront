import { UpdateComponent } from './update.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const UpdateRouting: Routes = [
  {
    path: '',
    component: UpdateComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(UpdateRouting)
  ],
  exports: [
    RouterModule
  ]
})
export class UpdateRoutingModule {
}

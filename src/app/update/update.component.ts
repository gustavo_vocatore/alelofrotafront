import { Vehicle } from './../models/vehicle';
import { MessageResponse } from './../models/message-response';
import { ShowAlertService } from './../services/show-alert.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VehicleService } from './../services/vehicle.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  updateForm: FormGroup;
  submitted = false;
  id: number;

   // convenience getter for easy access to form fields
   get f() { return this.updateForm.controls; }


  constructor(private route: ActivatedRoute,
              private spinner: NgxSpinnerService,
              private vehicleService: VehicleService,
              private formBuilder: FormBuilder,
              private showAlertService: ShowAlertService) { }

  ngOnInit(): void {
    this.updateForm = this.formBuilder.group({
      plate: this.formBuilder.control('', [Validators.required]),
      model: this.formBuilder.control('', [Validators.required]),
      manufacturer: this.formBuilder.control('', [Validators.required]),
      color: this.formBuilder.control('', [Validators.required]),
      status: this.formBuilder.control('true', [Validators.required]),
    });
    this.route.params
    .subscribe((parametro: any) => {
      this.id = parametro.id;
      this.findById(parametro.id);
    });
  }

  findById(id: number) {
    this.spinner.show();
    this.vehicleService.findById(id)
      .subscribe((vehicle: Vehicle) => {
        this.spinner.hide();
        this.updateForm.get('plate').setValue(vehicle.plate);
        this.updateForm.get('model').setValue(vehicle.model);
        this.updateForm.get('manufacturer').setValue(vehicle.manufacturer);
        this.updateForm.get('color').setValue(vehicle.color);
        this.updateForm.get('status').setValue(vehicle.status.toString());
     });
  }

  onSubmit() {
    this.submitted = true;
    if (this.updateForm.invalid) {
        return;
    }
    this.update(this.updateForm.value);
  }

  update(updateForm: any) {
    this.spinner.show();
    const vehicle: Vehicle = new Vehicle(updateForm);
    this.vehicleService.update(vehicle, this.id)
      .subscribe((messageResponse: MessageResponse) => {
        this.showAlertService.showAlert(messageResponse.message, '#088A4B');
        this.spinner.hide();
     });
  }

}

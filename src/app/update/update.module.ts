import { UpdateComponent } from './update.component';
import { UpdateRoutingComponent } from './update-routing.component';
import { UpdateRoutingModule } from './update-routing.module';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    SharedModule.forRoot(),
    UpdateRoutingModule
  ],
  declarations: [
    UpdateRoutingComponent,
    UpdateComponent
  ],
  providers: [
  ]
})
export class UpdateModule { }

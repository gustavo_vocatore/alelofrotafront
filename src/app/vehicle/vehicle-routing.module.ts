import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleComponent } from './vehicle.component';

export const VehicleRouting: Routes = [
  {
    path: '',
    component: VehicleComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(VehicleRouting)
  ],
  exports: [
    RouterModule
  ]
})
export class VehicleRoutingModule {
}

import { MessageResponse } from './../models/message-response';
import { Page } from './../models/page';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Vehicle } from '../models';
import { ShowAlertService, VehicleService } from '../services';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

  consultaForm: FormGroup;
  submitted = false;
  page: Page = new Page();
  imagem = '';
  status = '';
  active = 0;

   // convenience getter for easy access to form fields
   get f() { return this.consultaForm.controls; }

  constructor(private spinner: NgxSpinnerService,
              private vehicleService: VehicleService,
              private showAlertService: ShowAlertService,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.consultaForm = this.formBuilder.group({
      plate: this.formBuilder.control('', [Validators.required])
    });
    this.findByPage();
  }

  findByPage() {
    this.spinner.show();
    this.vehicleService.findByPage(this.active)
      .subscribe((page: Page) => {
        this.page = page;
        this.spinner.hide();
     });
  }

  onSubmit() {
    this.submitted = true;
    if (this.consultaForm.invalid) {
        return;
    }
    this.consulta(this.consultaForm.value);
  }

  consulta(consultaForm: any) {
    this.spinner.show();
    this.vehicleService.findByPlate(consultaForm.plate.toUpperCase())
      .subscribe((vehicles: Array<Vehicle>) => {
        this.page.content = vehicles;
        this.spinner.hide();
     });
  }

  getImagem(vehicle: Vehicle): string {
    if (vehicle.status) {
      return 'assets/imagem/Trafficlight-green-icon.png';
    }
    else {
      return 'assets/imagem/Button-Blank-Red-icon.png';
    }
  }

  getStatus(vehicle: Vehicle): string {
    if (vehicle.status) {
      return 'Active';
    }
    else {
      return 'Inactive';
    }
  }

  canActive(): number {
    return this.active;
  }

  disabledPrevious(): boolean {
    if (this.page.number === 0) {
      return true;
    }
    return false;
  }

  changePage(page?: number) {
    if (page >= 0  && page < this.page.totalPages) {
        this.active = page;
        this.findByPage();
    }
  }

  disabledNext(): boolean {
    return this.page.last;
  }

  delete(id: number){
    if (confirm('Are you sure to delete')) {
      this.vehicleService.delete(id)
      .subscribe((messageResponse: MessageResponse) => {
        this.showAlertService.showAlert(messageResponse.message, '#088A4B');
        this.changePage(0);
        this.spinner.hide();
     });
    }
  }

}

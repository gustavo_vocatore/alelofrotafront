import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { VehicleRoutingComponent } from './vehicle-routing.component';
import { VehicleRoutingModule } from './vehicle-routing.module';
import { VehicleComponent } from './vehicle.component';

@NgModule({
  imports: [
    SharedModule.forRoot(),
    VehicleRoutingModule
  ],
  declarations: [
    VehicleRoutingComponent,
    VehicleComponent
  ],
  providers: [
  ]
})
export class VehicleModule { }
